FROM debian:11.5
RUN apt-get update && apt-get install -y \
    curl \
    iputils-ping \
    vim \
    net-tools \
    openssh-client \
    zip \  
  && mkdir /scripts \
  && rm -rf /var/lib/apt/lists/* \
  && echo "alias toolcurl=/scripts/tool_curl.sh" >> /root/.bashrc
COPY ./scripts/* /scripts/

