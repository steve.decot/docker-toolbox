# docker-toolbox
## Description
Container debian image based with many tools : 
* curl : is a tool to transfer data from or to a server, using one of the supported protocols (DICT, FILE, FTP, FTPS,
       GOPHER, HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS, POP3, POP3S, RTMP, RTSP, SCP, SFTP, SMB, SMBS, SMTP, SMTPS, TELNET
       and TFTP). The command is designed to work without user interaction.
* iputils-ping : Tools to test the reachability of network hosts
* vim 
* net-tools : NET-3 networking toolkit --> arp, ifconfig, netstat, rarp, nameif et route
* openssh-client : secure shell (SSH) client, for secure access to remote machines
* zip

## Usage 
### Build container
Run the follow commands 

```
docker build . --tag image_name_of_image_you_like:version_name_what_you_want
```

### Execution
#### With no volumes
One shot time 

```
docker run --rm -ti toolbox:1  curl https://gitlab.com
```


## Authors 
Steve Decot - decot.steve@gmail.com
